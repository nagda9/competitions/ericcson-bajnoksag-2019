#include <array>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>
#include <set>
#include <cmath>
#include<bits/stdc++.h>

using namespace std;

bool newmessage = false;

struct CMD
{
    CMD() {}
    string command;
    unsigned router_id;
    char r_dir;
    unsigned int store_id;
};

enum class Direction : char
{
    LEFT = 'l',
    RIGHT = 'r'
};

struct Data
{
    unsigned int currRouter;
    unsigned int currStoreId;
    unsigned int dataIndex;
    unsigned int messageId;
    unsigned int fromRouter;
    unsigned int toRouter;
    Direction dir;
};

struct MessagePiece
{
    int index;
    string message;

    bool operator< (MessagePiece& x)    //rendezéshez kell
    {
        return index < x.index;
    }
};

struct Reader
{
    array<unsigned int, 3> data;
    string previous;
    array<array<bool, 10>, 14> routerBits;
    vector<Data> dataArray;
    vector<MessagePiece> receivedPieces;
    bool hasEnd;
};

struct CMD_parameters
{
    CMD_parameters() {}

    vector<CMD> cmd;
    int point;
    vector<Data> dataArray;
    array<array<bool, 10>, 14> routerBits;
    Direction next_data_dir;

};

void readData(Reader& to)
{
    string line;
    to.dataArray.clear();
    // to.receivedPieces.clear();

    while (getline(cin, line))
    {
        if (!line.rfind(".", 0))
        {
            return;
        }

        if (!line.rfind("WRONG", 0) ||
            !line.rfind("SCORE", 0) ||
            !line.rfind("TICK", 0) ||
            !line.rfind("SLOW", 0))
        {
            to.hasEnd = true;
            to.previous = move(line);
        }
        else if (!line.rfind("REQUEST", 0))
        {
            stringstream(move(line).substr(8)) >> to.data[0] >> to.data[1] >> to.data[2];
        }
        else if (!line.rfind("PREVIOUS", 0))
        {
            to.previous = move(line).substr(9);
        }
        else if (!line.rfind("ROUTER", 0))
        {
            unsigned int routerIndex;
            istringstream(line.substr(7)) >> routerIndex >> line;
            auto it = line.begin();
            for (bool& routers : to.routerBits[routerIndex])
                routers = *it++ == '1';
        }
        else if (!line.rfind("DATA", 0))
        {
            Data& curr = to.dataArray.emplace_back();
            istringstream(move(line).substr(5))
                >> curr.currRouter
                >> curr.currStoreId
                >> curr.dataIndex
                >> curr.messageId
                >> curr.fromRouter
                >> curr.toRouter
                >> reinterpret_cast<char&>(curr.dir);
        }
        else if (!line.rfind("MESSAGE"))
        {
            newmessage = true;
            MessagePiece& msg = to.receivedPieces.emplace_back();
            istringstream(move(line).substr(8)) >> msg.index >> msg.message;
        }
        else
        {
            cerr << "READER ERROR HAPPENED: unrecognized command line: " << line << endl;
            to.hasEnd = true;
            return;
        }
    }
    cerr << "Unexpected input end." << endl;
    to.hasEnd = true;
}

void permute(string a, int l, int r, set<string>& commands) //rekurzív
{
    // Base case
    if (l == r)
        commands.insert(a);
    else
    {
        // Permutations made
        for (int i = l; i <= r; i++)
        {

            // Swapping done
            swap(a[l], a[i]);

            // Recursion called
            permute(a, l+1, r, commands);

            //backtrack
            swap(a[l], a[i]);
        }
    }
}

string sort_pieces(vector<MessagePiece>& v)
{
    string msg = "";

    for (size_t i = 0; i < v.size()-1; i++)
    {
        for (size_t j = 0; j < v.size()-i-1; j++)
        {
            if (v[j+1] < v[j])
            {
                MessagePiece temp = v[j];
                v[j] = v[j+1];
                v[j+1] = temp;
            }
        }
    }

    for (size_t i = 0; i < v.size(); i++)
    {
        if (v[i].message[0] != ' ')
        {
            msg += v[i].message;
            cerr << v[i].index << " ";
        }
    }
    cerr << endl;

    return msg;
}

int count_my_data(const Reader& reader)
{
    int sum = 0;
    for (Data d : reader.dataArray)
    {
        if (d.fromRouter == reader.data[2])
        {
            sum++;
        }
    }
    return sum;
}

void move_router(array<array<bool, 10>, 14>& routerBits_changed, const char& r_dir, const unsigned int& movedRouter, const Reader& reader)
{
    for (unsigned int i = 0; i < reader.routerBits.size(); i++)
    {
        for (unsigned int j = 0; j < reader.routerBits[i].size(); j++)
        {
            if (i != movedRouter)
            {
                routerBits_changed[i][j] = reader.routerBits[i][j];
            }
            else
            {
                if (r_dir == '^')
                {
                    routerBits_changed[i][j] = reader.routerBits[i][(j+1)%10];
                }
                else if (r_dir == 'v')
                {
                    routerBits_changed[i][j] = reader.routerBits[i][(j-1)%10];
                }
            }

        }

    }
}

// Ez számol azzal, hogy az adatok előbb felmennek, és csak aztán a célirányba? már igen bibibiiii
void step_data(bool& can_move_up, bool& can_move_dir, unsigned int& m, const unsigned int& currStore, const unsigned int& currRouter, const Direction& d_dir, const array<array<bool, 10>, 14>& routerBits_changed, const Reader& reader)
{
    if (currStore == 0){can_move_up = false;} // ha 0. tárolón van az adat, akkor false
    else if (routerBits_changed[currRouter][currStore-1]==0) {can_move_up = false;}
    // ha zárva a felette levő, akkor false
    else // ha a felette levő tároló foglalt, akkor false
    {
        for (unsigned int i = 0; i < reader.dataArray.size(); i++)
        {
            if (reader.dataArray[i].currRouter == currRouter && reader.dataArray[i].currStoreId == currStore-1)
            {
                can_move_up = false;
            }
        }
    }

    // balra megy
    if (d_dir == Direction::LEFT)
    {
        if (currRouter == 0){m = 13;}
        else {m = currRouter-1;}
    }
    //Jobbra megy
    else
    {
        if (currRouter == 13){m = 0;}
        else {m = currRouter+1;}
    }

    if (routerBits_changed[m][currStore] == 0){can_move_dir = false;}
    // mellette levő tároló zárt
    else
    // mellette levő tároló foglalt
    {
        for (unsigned int i = 0; i < reader.dataArray.size(); i++)
        {
            if (reader.dataArray[i].currRouter == m && reader.dataArray[i].currStoreId == currStore)
            {
                can_move_dir = false;
            }
        }
    }
}

array<unsigned int, 2> simulate_data_move(Reader reader, unsigned int currRouter, unsigned int currStore, Direction d_dir,  array<array<bool, 10>, 14> routerBits_changed, unsigned int torouter) // simulate move esetén
{
    bool can_move_up=true;
    bool can_move_dir=true;
    unsigned int m;

    step_data(can_move_up, can_move_dir, m, currStore, currRouter, d_dir, routerBits_changed, reader);

    while (can_move_up == true || can_move_dir == true)
    {
        can_move_up=true;
        can_move_dir=true;

        step_data(can_move_up, can_move_dir, m, currStore, currRouter, d_dir, routerBits_changed, reader);

        if(can_move_up == true ){currStore --;}
        if(can_move_up ==false && can_move_dir == true ){currRouter = m; if (m == torouter){break;} }
    }
array<unsigned int, 2> v;
v[0]=currRouter;
v[1]=currStore;
return v;
}

array<unsigned int, 2> simulate_data_move(Reader reader, unsigned int currRouter, unsigned int currStore, Direction d_dir) // simulate create esetén (2 paraméterrel kevesebb)
{
    bool can_move_up=true;
    bool can_move_dir=true;
    unsigned int m;

    step_data(can_move_up, can_move_dir, m, currStore, currRouter, d_dir, reader.routerBits, reader);

    while (can_move_up == true || can_move_dir == true)
    {
        can_move_up=true;
        can_move_dir=true;

        step_data(can_move_up, can_move_dir, m, currStore, currRouter, d_dir, reader.routerBits, reader);

        if(can_move_up == true ){currStore --;}
        if(can_move_up ==false && can_move_dir == true ){currRouter = m;}
    }
array<unsigned int, 2> v;
v[0]=currRouter;
v[1]=currStore;
return v;
}

void rekurziv(const string& str, // vizsgált cmd szekvencia sorrend
                    unsigned int index, // vizsgált cmd indexe
                    vector<CMD_parameters>& w, // cmd-k átváltása rendes cmd-re
                    vector<pair<vector<CMD>, int>>& command_seq,
                    const Reader& reader,
                    const Direction& next_data_dir)
{
    vector<CMD_parameters> v;
    const int s = w.size();
    if (str[index] == 'C')
    {
        for (size_t h = 0; h < max(s, 1); h++) // mindenképp lefut 1-szer
        {
            // innentől másolt
            if (count_my_data(reader) < 4) // ha kevesebb mint 4 adatunk van...
            {
                for (unsigned int i = 0; i < 10; i++) // végigmegyünk a saját routerünkön a tárolókon
                {
                    bool empty = true;
                    for (unsigned int j = 0; j < reader.dataArray.size(); j++) // megnézzük, hogy a tároló üres-e
                    {
                        if (reader.dataArray[j].currRouter == reader.data[2] && reader.dataArray[j].currStoreId == i)
                        {
                            empty = false;
                        }
                    }

                    if (empty == true && reader.routerBits[reader.data[2]][i] == true) // ha a tároló üres és nyitva van
                    {
                        // már nem az első rekurzió
                        if (w.size() > 0)
                        {
                            CMD cmd;
                            cmd.command = "CREATE";
                            cmd.store_id = i;

                            CMD_parameters uj;
                            uj = w[h];
                            uj.cmd.push_back(cmd);

                            // itt a szimulációnak a w[h] mezőit adjuk
                            array<unsigned int, 2> data_new_pos = simulate_data_move(reader, reader.data[2], i, next_data_dir);
                                // 2 elemű tömb a router és tároló számának
                            // uj.point
                            uj.point += (reader.data[2] - data_new_pos[0]) % 14;
                            // uj.dataArray

                            v.push_back(uj);
                        }
                        // első rekurzió
                        else
                        {
                            CMD cmd;
                            cmd.command = "CREATE";
                            cmd.store_id = i;

                            CMD_parameters uj;
                            uj.cmd.push_back(cmd);
                            uj.routerBits = reader.routerBits;
                            uj.dataArray = reader.dataArray;
                            uj.next_data_dir = next_data_dir;

                            // itt a szimulációnak a reader dolgait adjuk
                            array<unsigned int, 2> data_new_pos = simulate_data_move(reader, reader.data[2], i, next_data_dir);
                                // 2 elemű tömb a router és tároló számának
                            // uj.point
                            uj.point += (reader.data[2] - data_new_pos[0]) % 14;
                            // uj.dataArray pushbackeljük a létrehozandó adatot

                            v.push_back(uj);
                        }
                    }
                }
            }
            // ha Create a command, és nem tudunk létrehozni több adatot
            else
            {
///////////////////////////////////////////////////////
                // már nem az első rekurzió
                if (w.size() > 0)
                {
                    CMD cmd;

                    cmd.command = "PASS";

                    CMD_parameters uj;
                    uj = w[h];
                    uj.cmd.push_back(cmd);
                    v.push_back(uj);
                    // kész
                }
                // az első rekurzió (meg kell különböztetni, mert fel kell tölteni a mezőket)
                else
                {
                    CMD cmd;

                    cmd.command = "PASS";

                    CMD_parameters uj;
                    uj.cmd.push_back(cmd);
                    uj.point = 0;
                    uj.routerBits = reader.routerBits;
                    uj.dataArray = reader.dataArray;
                    uj.next_data_dir = next_data_dir;
                    v.push_back(uj);
                    // kész
                }
            }
        }
    }
///////////////////////////////////////////////////////
    if (str[index] == 'M')
    {
        for (size_t h = 0; h < max(s, 1); h++)
        {
            // innentől másolt
            for (unsigned int i = 0; i < reader.routerBits.size(); i++)
            {
                vector<Data> on_router;
                vector<Data> next_to_router;
                vector<Data> on_router_opponent;

                for(unsigned int j = 0; j < reader.dataArray.size(); j++)
                {
                    if (reader.dataArray[j].fromRouter == reader.data[2] && i == reader.dataArray[j].currRouter)
                    {
                        on_router.push_back(reader.dataArray[j]);
                    }
                    else if (reader.dataArray[j].fromRouter != reader.data[2] && i == reader.dataArray[j].currRouter)
                    {
                        on_router_opponent.push_back(reader.dataArray[j]);
                    }

                    if (reader.dataArray[j].fromRouter == reader.data[2] && ((reader.dataArray[j].currRouter == (i-1) % 14 ) or (reader.dataArray[j].currRouter == (i+1) % 14 )))
                    {
                        next_to_router.push_back((reader.dataArray[j]));
                    }
                }

                if (on_router.size() > 0)
                {
                    // már nem az első rekurzió
                    if (w.size() > 0)
                    {                           
                        CMD cmd_up;
                        cmd_up.command = "MOVE";
                        cmd_up.router_id = i;
                        cmd_up.r_dir = '^';

                        CMD cmd_down;
                        cmd_down.command = "MOVE";
                        cmd_down.router_id = i;
                        cmd_down.r_dir = 'v';

                        CMD_parameters uj_up;
                        CMD_parameters uj_down;

                        uj_up = w[h];
                        uj_down = w[h];

                        uj_up.cmd.push_back(cmd_up);
                        uj_down.cmd.push_back(cmd_down);

                        // itt a szimulációnak a w[h] mezőit adjuk
                        for (unsigned int j = 0; j < on_router.size(); j++)
                        {
                            array<array<bool, 10>, 14>  routerBits_changed;
                            move_router(routerBits_changed,'^', i, reader);

                            array<unsigned int, 2> data_new_pos_up = simulate_data_move(reader, i, (on_router[j].currStoreId - 1) % 10, on_router[j].dir,routerBits_changed, on_router[j].toRouter);
                            for (unsigned int k = 0; k < next_to_router.size(); k++)
                            {
                                if (next_to_router[k].currRouter == (i-1) % 14 or next_to_router[k].currRouter == (i+1) % 14)
                                {
                                    array<unsigned int, 2> data_new_pos_up_szomszed = simulate_data_move(reader, next_to_router[k].currRouter, next_to_router[k].currStoreId, on_router[j].dir, routerBits_changed, next_to_router[k].toRouter);

                                    // uj_up point hozzáadás
                                    uj_up.point += (next_to_router[k].currRouter - data_new_pos_up_szomszed[0]) % 14;
                                }
                            }

                            move_router(routerBits_changed,'v', i, reader);

                            array<unsigned int, 2> data_new_pos_down = simulate_data_move(reader, i, (on_router[j].currStoreId + 1) % 10, on_router[j].dir, routerBits_changed, on_router[j].toRouter);
                            for (unsigned int k = 0; k < next_to_router.size(); k++)
                            {
                                if (next_to_router[k].currRouter == (i-1) % 14 or next_to_router[k].currRouter == (i+1) % 14)
                                {
                                    array<unsigned int, 2> data_new_pos_down_szomszed = simulate_data_move(reader, next_to_router[k].currRouter, next_to_router[k].currStoreId, on_router[j].dir,routerBits_changed, next_to_router[k].toRouter);

                                    // uj_down point hozzáadás
                                    uj_down.point += (next_to_router[k].currRouter - data_new_pos_down_szomszed[0]) % 14;
                                }
                            }

                            // uj pontot hozzáadom
                            uj_up.point += (i - data_new_pos_up[0]) % 14;
                            uj_down.point += (i - data_new_pos_down[0]) % 14;
                        }

                        // megváltoztatom a dataArrayt helyét
                        // megváltoztatom a routerBiteket

                        v.push_back(uj_up);
                        v.push_back(uj_down);
                    }

                    // az első rekurzió
                    else
                    {
                        CMD cmd_up;
                        cmd_up.command = "MOVE";
                        cmd_up.router_id = i;
                        cmd_up.r_dir = '^';

                        CMD cmd_down;
                        cmd_down.command = "MOVE";
                        cmd_down.router_id = i;
                        cmd_down.r_dir = 'v';

                        CMD_parameters uj_up;
                        CMD_parameters uj_down;

                        uj_up.cmd.push_back(cmd_up);
                        uj_down.cmd.push_back(cmd_down);

                        // itt a szimulációnak a reader dolgait adjuk
                        for (unsigned int j = 0; j < on_router.size(); j++)
                        {
                            array<array<bool, 10>, 14>  routerBits_changed;
                            move_router(routerBits_changed,'^', i, reader);

                            array<unsigned int, 2> data_new_pos_up = simulate_data_move(reader, i, (on_router[j].currStoreId - 1) % 10, on_router[j].dir,routerBits_changed, on_router[j].toRouter);
                            for (unsigned int k = 0; k < next_to_router.size(); k++)
                            {
                                if (next_to_router[k].currRouter == (i-1) % 14 or next_to_router[k].currRouter == (i+1) % 14)
                                {
                                    array<unsigned int, 2> data_new_pos_up_szomszed = simulate_data_move(reader, next_to_router[k].currRouter, next_to_router[k].currStoreId, on_router[j].dir, routerBits_changed, next_to_router[k].toRouter);

                                    // uj_up point hozzáadás
                                    uj_up.point += (next_to_router[k].currRouter - data_new_pos_up_szomszed[0]) % 14;
                                }
                            }

                            move_router(routerBits_changed,'v', i, reader);

                            array<unsigned int, 2> data_new_pos_down = simulate_data_move(reader, i, (on_router[j].currStoreId + 1) % 10, on_router[j].dir, routerBits_changed, on_router[j].toRouter);
                            for (unsigned int k = 0; k < next_to_router.size(); k++)
                            {
                                if (next_to_router[k].currRouter == (i-1) % 14 or next_to_router[k].currRouter == (i+1) % 14)
                                {
                                    array<unsigned int, 2> data_new_pos_down_szomszed = simulate_data_move(reader, next_to_router[k].currRouter, next_to_router[k].currStoreId, on_router[j].dir,routerBits_changed, next_to_router[k].toRouter);

                                    // uj_down point hozzáadás
                                    uj_down.point += (next_to_router[k].currRouter - data_new_pos_down_szomszed[0]) % 14;
                                }
                            }

                            // uj pontot hozzáadom
                            uj_up.point += (i - data_new_pos_up[0]) % 14;
                            uj_down.point += (i - data_new_pos_down[0]) % 14;
                        }

                        // megváltoztatom a dataArrayt helyét
                        // megváltoztatom a routerBiteket

                        v.push_back(uj_up);
                        v.push_back(uj_down);
                    }
                }
            }
        }
    }

    index++;
    if(index == str.size())
    {
        for (unsigned int i = 0; i < v.size(); i++)
        {
            if (v[i].cmd.size() == str.size())
            {
                pair<vector<CMD>, int> new_pair (v[i].cmd, v[i].point);
                command_seq.push_back(new_pair);
            }
        }
        return;
    }
    else
    {
        rekurziv(str, index, v, command_seq, reader, next_data_dir);
    }
}

CMD maxkeres (vector<pair<vector<CMD>, int>> command_seq)
{
    CMD best_cmd;

    int max = 0;
    int index = 0;
    for (size_t i = 0; i < command_seq.size(); i++)
    {
        if (command_seq[i].second > max)
        {
            max = command_seq[i].second;
            index = i;
        }
    }

    best_cmd = command_seq[index].first[0];

    return best_cmd;
}

int main()
{
    char teamToken[] = "4q-APz-qLreKwF7-VyBC";
    int seed = 555;
    string version = "version2";

    cout << "START " << teamToken
        << " " << seed
        << " " << version
        << endl;

    Reader reader = {};

    string command, final_msg;
    char r_dir = '^';
    unsigned int router_id, store_id, package_id = 0;

    int d_dir = 0;
    Direction next_data_dir = Direction::LEFT;


    while(true)
    {
        readData(reader);
        if (reader.hasEnd)
            break;

//----------------------------------------------------------------------
        // decide command

        if (newmessage == true && reader.receivedPieces.size() != 0 && reader.receivedPieces.back().message == "")
        {
            command = "SOLUTION";
            final_msg = sort_pieces(reader.receivedPieces);
        }
        else
        {
            const int n = 3; // előre látás
            set<string> commands;

            for (int i = 0; i <= min(4, n); i++)
            {
                string cmd = "";

                for (int j = 0; j < i; j++)
                {
                    cmd += 'C';
                }
                for (int j = i; j < n; j++)
                {
                    cmd += 'M';
                }

                permute(cmd, 0, n-1, commands);
            }

            vector<CMD_parameters> v;
            vector<pair<vector<CMD>, int>> command_seq;

            for (string s : commands)
            {
                rekurziv(s, 0, v, command_seq, reader, next_data_dir);
            }

            CMD best_cmd = maxkeres(command_seq);

            if (best_cmd.command == "PASS")
            {
                command = best_cmd.command;
            }
            else if (best_cmd.command == "CREATE")
            {
                command = best_cmd.command;
                store_id = best_cmd.store_id;
            }
            else if (best_cmd.command == "MOVE")
            {
                command = best_cmd.command;
                router_id = best_cmd.router_id;
                r_dir = best_cmd.r_dir;
            }
        }
//----------------------------------------------------------------------

        // send command
        if (command == "PASS") {
            cout << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command << endl;
            cerr << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command << endl;
        }
        else if (command == "CREATE") {
            cout << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command
                    << " " << store_id
                    << " " << package_id << endl;
            cerr << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command
                    << " " << store_id
                    << " " << package_id << endl;
            package_id += 1;

            d_dir = (d_dir + 1) % 2;
            if (d_dir == 0)
            {
                next_data_dir = Direction::LEFT;
            }
            else
            {
                next_data_dir = Direction::RIGHT;
            }
        }
        else if (command == "MOVE") {
            cout << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command
                    << " " << router_id
                    << " " << r_dir << endl;
            cerr << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command
                    << " " << router_id
                    << " " << r_dir << endl;
        }
        else if (command == "SOLUTION") {
            cout << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command
                    << " " << final_msg << endl;
            cerr << reader.data[0]
                    << " " << reader.data[1]
                    << " " << reader.data[2]
                    << " " << command
                    << " " << final_msg << endl;
        }
        newmessage = false;
    }
    cerr << "END (latest message): " << reader.previous << endl;
}
